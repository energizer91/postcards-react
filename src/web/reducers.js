/**
 * Created by energizer on 27.03.17.
 */
import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import headerMenu from './components/Header/reducers';
import slideShow from './components/SlideShow/reducers';
import buy from './containers/Buy/reducers';

export default combineReducers({
    headerMenu,
    slideShow,
    buy,
    routing
});
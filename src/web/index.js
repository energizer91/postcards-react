import React from 'react';
import ReactDOM from 'react-dom';
import './stylesReset.css';

import { Router, browserHistory } from 'react-router';
import { createStore, applyMiddleware, compose } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import routes from './containers/routes';
import reducers from './reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(thunk));

const store = createStore(
    reducers,
    enhancer
);

const history = syncHistoryWithStore(browserHistory, store);

const createElement = (Component, props) => {
    return <Component key={`${props.route.key || props.route.path || 'random'}RouteComponent`} {...props}/>
};

ReactDOM.render(
    <Provider store={store}>
        <Router history={history} routes={routes} createElement={createElement} />
    </Provider>,
  document.getElementById('root')
);

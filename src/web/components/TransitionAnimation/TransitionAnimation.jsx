import React from 'react';
import MenuTextWrapper from '../MenuTextWrapper/MenuTextWrapper';

import './TransitionAnimation.css';

const TransitionAnimation = () => (
    <div id="transitionAnimation">
        <div className="inner menu-item">
            <div className="image" />
            <MenuTextWrapper />
        </div>
    </div>
);

export default TransitionAnimation;
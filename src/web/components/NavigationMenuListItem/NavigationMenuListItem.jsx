import React from 'react';
import MainMenuListItem from '../MainMenuListItem/MainMenuListItem';

import './NavigationMenuListItem.css';
import './responsive/portrait_360.css';
import './responsive/landscape_360.css';
import './responsive/portrait_768.css';
import './responsive/landscape_768.css';
import './responsive/landscape_1366.css';
import './responsive/landscape_1600.css';
import './responsive/landscape_1920.css';

const NavigationMenuListItem = ({handleClick,className, title, subTitle, path}) => (
    <MainMenuListItem className={['navigation', className]} title={title} subTitle={subTitle} path={path} handleClick={handleClick} />
);

export default NavigationMenuListItem;
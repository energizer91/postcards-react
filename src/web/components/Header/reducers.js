/**
 * Created by Александр on 31.03.2017.
 */
import { SHOW_HEADER_MENU, HIDE_HEADER_MENU, START_HEADER_ANIMATION, STOP_HEADER_ANIMATION, SET_ACTIVE_MENU } from './constants';

export default function headerMenu (state = {showMenu: false, pendingAnimation: false, path: ''}, action) {
    switch (action.type) {
        case SHOW_HEADER_MENU:
        case HIDE_HEADER_MENU:
            return Object.assign({}, state, {
                showMenu: action.showMenu
            });
        case START_HEADER_ANIMATION:
        case STOP_HEADER_ANIMATION:
            return Object.assign({}, state, {
                pendingAnimation: action.pendingAnimation
            });
        case SET_ACTIVE_MENU:
            return Object.assign({}, state, {
                path: action.path
            });
        default:
            return state;
    }
}
/**
 * Created by Александр on 31.03.2017.
 */
import { SHOW_HEADER_MENU, HIDE_HEADER_MENU, START_HEADER_ANIMATION, STOP_HEADER_ANIMATION, SET_ACTIVE_MENU } from './constants';
import { TimelineMax, Expo } from 'gsap';

function toggleMenu (value) {
    return {
        type: value ? SHOW_HEADER_MENU : HIDE_HEADER_MENU,
        showMenu: value
    }
}

function toggleAnimation (value) {
    return {
        type: value ? START_HEADER_ANIMATION : STOP_HEADER_ANIMATION,
        pendingAnimation: value
    }
}

export function showHeaderMenu () {
    return function (dispatch) {
        const menuItems = document.querySelectorAll('.navigation.menu-item .wrapper'),
            menu = document.getElementById('menu'),
            logo = document.getElementsByClassName('logo'),
            tl = new TimelineMax({
                onStart: () => (dispatch(toggleAnimation(true))),
                onComplete: () => (dispatch(toggleAnimation(false)))
            });

        document.body.classList.add('is-menu');
        tl.set(menu, {autoAlpha: 1});
        tl.to(logo, 0.4, {autoAlpha: 0});
        tl.staggerTo(menuItems, 0.7, {x: '0%'}, 0.15, 0);
        tl.restart();
        dispatch(toggleMenu(true));
    }
}

export function hideHeaderMenu () {
    return function (dispatch) {
        const menuItems = document.querySelectorAll('.navigation.menu-item .wrapper'),
            menu = document.getElementById('menu'),
            logo = document.getElementsByClassName('logo'),
            tl = new TimelineMax({
                onStart: () => (dispatch(toggleAnimation(true))),
                onComplete: () => (dispatch(toggleAnimation(false)))
            });

        document.body.classList.remove('is-menu');
        tl.set(menu, {background: "transparent", "pointer-events": "none"});
        tl.staggerTo(menuItems, 1.4, {x: '100%', ease: Expo.easeInOut}, 0.08, 0.4);
        tl.to(logo, 0.4, {autoAlpha: 1, clearProps: 'all'}, 0);
        tl.set(menu, {autoAlpha: 0, clearProps: 'all'});
        tl.set(menuItems, {clearProps: 'all'});
        tl.restart();
        dispatch(toggleMenu(false));
    }
}

export function setActiveMenu (path) {
    return {
        type: SET_ACTIVE_MENU,
        path
    }
}
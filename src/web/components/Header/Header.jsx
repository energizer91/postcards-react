import React from 'react';
import logo from './logo.png';
import { Link } from 'react-router';
import Hamburger from '../Hamburger/Hamburger';
import Menu from '../Menu/Menu';
import { connect } from 'react-redux';
import { showHeaderMenu, hideHeaderMenu } from './actions';

import './Header.css';

import './responsive/landscape_1366.css';

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.toggleMenu = this.toggleMenu.bind(this);
    }

    showMenu() {
        this.props.dispatch(showHeaderMenu());
    }

    hideMenu() {
        this.props.dispatch(hideHeaderMenu());
    }

    toggleMenu() {
        if (!this.props.pendingAnimation) {
            if (this.props.showMenu) {
                this.hideMenu();
            } else {
                this.showMenu();
            }
        }
    }

    render() {
        return (
            <div>
                <div className="header">
                    <Link to="/" className="logo">
                        <img src={logo} alt=""/>
                    </Link>
                    <Hamburger extraClassName="navigation" onClick={this.toggleMenu} showCross={this.props.showMenu}/>
                </div>
                <Menu onClick={this.hideMenu} />
            </div>
        )
    }
}

export default connect(state => state.headerMenu)(Header);
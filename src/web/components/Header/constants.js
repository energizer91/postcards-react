/**
 * Created by Александр on 31.03.2017.
 */

export const SHOW_HEADER_MENU = 'SHOW_HEADER_MENU';
export const HIDE_HEADER_MENU = 'HIDE_HEADER_MENU';
export const START_HEADER_ANIMATION = 'START_HEADER_ANIMATION';
export const STOP_HEADER_ANIMATION = 'STOP_HEADER_ANIMATION';
export const SET_ACTIVE_MENU = 'SET_ACTIVE_MENU';
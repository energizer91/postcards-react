/**
 * Created by Александр on 22.04.2017.
 */

import React from 'react';

import './Contacts.css';

const Contacts = ({leftText, rightText, leftStyle, rightStyle}) => (
    <div className="contacts-block" style={leftStyle}>
        <div className="description">
            <div className="vertical-wrapper">
                <div className="vertical-cell">
                    {leftText}
                </div>
            </div>
        </div>
        <div className="contacts" style={rightStyle}>
            <div className="vertical-wrapper">
                <div className="vertical-cell">
                    {rightText}
                </div>
            </div>
        </div>
    </div>
);

Contacts.propTypes = {
    leftText: React.PropTypes.any.isRequired,
    rightText: React.PropTypes.any.isRequired,
    leftStyle: React.PropTypes.object,
    rightStyle: React.PropTypes.object
};

export default Contacts;
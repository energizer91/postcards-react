/**
 * Created by energizer on 26.04.17.
 */

import React from 'react';

import SmoothTextBlock from '../SmoothTextBlock/SmoothTextBlock';

import './MainMenuHeading.css';

const MainMenuHeading = ({title}) => (
    <div>
        <div className="main-bkg background-element">
            <div className="background" />
        </div>
        <div className="title-container">
            <h2 className="title">
                <SmoothTextBlock lines={title}/>
            </h2>
        </div>
    </div>
);

MainMenuHeading.propTypes = {
    title: React.PropTypes.array.isRequired
};

export default MainMenuHeading;
import React from 'react';


import './AnnotateButton.css';

const AnnotateButton = ({onClick, imageStyle, style, rotation, annotations, children, extraClassName}) => {
    const classList = ['annotate-button'];

    switch (rotation) {
        case '180':
            classList.push('rotate180');
            break;
        case '90':
            classList.push('rotate90');
            break;
        default:
            break;
    }

    return (
        <div className={classList.concat(extraClassName).join(' ')} onClick={onClick} style={style}>
            {(children || null) || (<div className="image-wrapper" style={imageStyle}/>)}
            {(annotations || []).map((annotation, index) => (
                <div key={index} className={['title-wrapper'].concat(annotation.className).join(' ')}>
                    <div className="title">
                        <span>{annotation.text}</span>
                    </div>
                </div>
            ))}
        </div>
    )
};

export default AnnotateButton;
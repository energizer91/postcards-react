/**
 * Created by Александр on 19.04.2017.
 */

import React from 'react';
import propTypes from 'prop-types';
import { Map as YMap, Marker } from 'yandex-map-react';
//import { GoogleMapLoader, GoogleMap, Marker,} from "react-google-maps";

import './Map.css';

class Map extends React.Component {
    constructor(props) {
        super(props);

        this.map = null;
    }

    componentDidMount() {
        //GoogleMapsLoader.KEY = 'AIzaSyA6BEeg-W9eNKxk2gabgdL0ReYcgzZQEz8';
    }

    render() {
        return (
            <div style={{width: '100%', height: '100%'}}>
                <YMap
                    width={'100%'}
                    height={'100%'}
                    zoom={this.props.geoData.zoom}
                    center={[this.props.geoData.lat, this.props.geoData.lon]}
                    >
                    {this.props.geoData.points.map((marker, index) => (
                        <Marker key={index} {...marker} />
                    ))}
                </YMap>
            </div>
        )
    }
}

Map.propTypes = {
    geoData: propTypes.shape({
        name: propTypes.string.isRequired,
        lat: propTypes.number.isRequired,
        lon: propTypes.number.isRequired,
        zoom: propTypes.number.isRequired,
        points: propTypes.arrayOf(propTypes.shape({
            lat: propTypes.number.isRequired,
            lon: propTypes.number.isRequired,
            properties: propTypes.shape({
                balloonContentHeader: propTypes.string.isRequired,
                balloonContentBody: propTypes.string.isRequired
            }).isRequired
        }))
    })
};

Map.defaultProps = {
    geoData: {
        name: '',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    }
};

export default Map;
import React from 'react';
import NavigationMenuListItem from '../NavigationMenuListItem/NavigationMenuListItem';
import { hideHeaderMenu, setActiveMenu } from '../Header/actions';
import { connect } from 'react-redux';

import './Menu.css';

class Menu extends React.Component {
    constructor(props) {
        super(props);

        this.goToMenu = this.goToMenu.bind(this);
    }

    goToMenu(path) {
        this.props.dispatch(setActiveMenu(path));
        this.props.dispatch(hideHeaderMenu());
    }

    getNavigation() {
        const items = [
            {
                className: 'buy',
                path: '/buy',
                title: 'Где купить открытку?',
                subTitle: 'Узнай продаются ли Новые Открытки в твоем городе.'
            },
            {
                className: 'authors',
                path: '/authors',
                title: <div><span>Сотрудничество</span> <span>с авторами</span></div>,
                subTitle: 'Мы работаем только с лучшими художниками. Присоединяйся к нашей команде!'
            },
            {
                className: 'partnership',
                path: '/partnership',
                title: 'Приятное партнёрство',
                subTitle: 'Наши уникальные стойки с открытками в ваших точках продаж.'
            }
        ];

        return items.map((item, i) => (
            <NavigationMenuListItem {...item} key={i} name="navigation" handleClick={this.goToMenu} />
        ))
    }

    render() {
        return (
            <div id="menu">
                <ul>
                    {this.getNavigation()}
                </ul>
            </div>
        )
    }
}

export default connect(state => state)(Menu);
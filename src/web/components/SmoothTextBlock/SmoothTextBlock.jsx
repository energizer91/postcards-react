/**
 * Created by energizer on 29.04.17.
 */

import React from 'react';

import './SmoothTextBlock.css';

class SmoothTextBlock extends React.Component {
    render() {
        const letters = this.props.lines.map((line, lineIndex) => (
            <span className="line" key={lineIndex}>
                {line.split(' ').map((word, wordIndex) => (
                    <div key={wordIndex} className="word">
                        {word.split('').map((letter, letterIndex) => (
                            <div key={letterIndex} className="letter">
                                {letter}
                            </div>
                        ))}&nbsp;
                    </div>
                ))}
            </span>
        ));

        return (
            <div className="smooth-text-block">
                {letters}
            </div>
        )
    }
}

export default SmoothTextBlock;
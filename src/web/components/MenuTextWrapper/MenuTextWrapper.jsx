import React from 'react';

const MenuTextWrapper = ({title, subTitle}) => (
    <div className="text-border">
        <div className="text">
            <div className="title">{title || null}</div>
            <div className="sub-title">
                <div className="sub-title-inner">
                    <p>{subTitle || null}</p>
                </div>
            </div>
        </div>
    </div>
);

export default MenuTextWrapper;
import React from 'react';
import AnnotateButton from '../AnnotateButton/AnnotateButton';
import ArrowImage from './images/arrow.png';


import './Arrow.css';

const imageDefaultStyle = {
    backgroundImage: 'url(' + ArrowImage + ')',
    width: 45,
    height: 45,
    backgroundSize: 45
};

const Arrow = ({onClick, rotation, style, imageStyle, annotation}) => (
    <AnnotateButton imageStyle={Object.assign({}, imageDefaultStyle, imageStyle)} rotation={rotation} onClick={onClick} style={style} annotations={[{text: annotation}]}/>
);

Arrow.propTypes = {
    onClick: React.PropTypes.func.isRequired,
    rotation: React.PropTypes.string,
    imageStyle: React.PropTypes.object,
    style: React.PropTypes.object,
    annotation: React.PropTypes.string
};

export default Arrow;
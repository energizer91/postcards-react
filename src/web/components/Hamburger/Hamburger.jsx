import React from 'react';
import AnnotateButton from '../AnnotateButton/AnnotateButton';

import './Hamburger.css';

const hamburgerPile = () => {
    const piles = [];
    for (let i = 0; i < 3; i++) {
        piles.push(<div className="hamburger-pile" key={i}/>);
    }

    return piles;
};

const annotations = [
    {
        text: 'Меню',
        className: 'menu'
    },
    {
        text: 'Закрыть',
        className: 'close'
    }
];



const Hamburger = ({extraClassName, showCross, onClick}) => (
    <AnnotateButton annotations={annotations} onClick={onClick} extraClassName={extraClassName}>
        <div id="hamburger" className={showCross ? 'is-menu' : null}>
            {hamburgerPile()}
        </div>
    </AnnotateButton>
);

export default Hamburger;
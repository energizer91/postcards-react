import React from 'react';
import { Link } from 'react-router';
import MenuTextWrapper from '../MenuTextWrapper/MenuTextWrapper';

import './MainMenuListItem.css';
import './responsive/portrait_360.css';
import './responsive/landscape_360.css';
import './responsive/portrait_768.css';
import './responsive/landscape_768.css';
import './responsive/landscape_1366.css';
import './responsive/landscape_1600.css';
import './responsive/landscape_1920.css';

const MainMenuListItem = ({className, title, subTitle, path, handleClick, centerOffset, scaleFactor}) => {
    const styles = {
        backgroundPositionX: centerOffset + '%',
        transform: 'scale(' + scaleFactor + ')'
    };

     return (
         <li className={['inner', 'menu-item'].concat(className).join(' ')}
             data-centerset={centerOffset}
             onClick={handleClick ? handleClick.bind(this, path) : null}
         >
            <div className="wrapper">
                <Link to={path} className="link">
                    <div className="image" style={styles}/>
                    <MenuTextWrapper title={title} subTitle={subTitle}/>
                </Link>
            </div>
        </li>
     );
};

export default MainMenuListItem;
/**
 * Created by Александр on 11.04.2017.
 */

import { LOAD_SLIDES, SWITCH_SLIDE } from './constants';

export default function slideShow (state = {slides: [], current: -1}, action) {
    switch (action.type) {
        case LOAD_SLIDES:
            return Object.assign({}, state, {
                slides: action.slides,
                current: 0
            });
        case SWITCH_SLIDE:
            return Object.assign({}, state, {
                current: action.index
            });
        default:
            return state;
    }
}
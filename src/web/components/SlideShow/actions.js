/**
 * Created by Александр on 11.04.2017.
 */

import { LOAD_SLIDES, SWITCH_SLIDE } from './constants';

export function loadSlides (slides) {
    return {
        type: LOAD_SLIDES,
        slides
    }
}

export function changeSlide (index) {
    return {
        type: SWITCH_SLIDE,
        index
    }
}
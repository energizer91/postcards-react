import React from 'react';
import {connect} from 'react-redux';
import {loadSlides, changeSlide} from './actions';
import Slide from '../Slide/Slide';
import Arrow from '../Arrow/Arrow';

import './SlideShow.css';

class SlideShow extends React.Component {
    constructor(props) {
        super(props);

        this.nextSlide = this.nextSlide.bind(this);
        this.previousSlide = this.previousSlide.bind(this);
    }

    nextSlide () {
        let index = this.props.current + 1;

        if (index > this.props.slides.length - 1) {
            index = 0;
        }
        return this.props.dispatch(changeSlide(index));
    }

    previousSlide () {
        let index = this.props.current - 1;

        if (index < 0) {
            index = this.props.slides.length - 1;
        }
        return this.props.dispatch(changeSlide(index));
    }

    componentWillMount () {
        return this.props.dispatch(loadSlides(this.props.data));
    }

    render() {
        let offset = 0;

        if (this.container) {
            offset = - this.container.getBoundingClientRect().width * this.props.current
        }

        return (
            <div className="slide-show" ref={el => this.container = el}>
                <div className="slide-wrapper" style={{transform: 'translateX(' + offset + 'px)'}}>
                    {this.props.slides.map((slide, index) => (
                        <Slide key={index} {...slide} />
                    ))}
                </div>
                <div className="controls">
                    <Arrow onClick={this.previousSlide} rotation="180" annotation="Назад"/>
                    <Arrow onClick={this.nextSlide} style={{float: 'right'}} imageStyle={{transform: 'scale(-1)'}} annotation="Вперед"/>
                </div>
            </div>
        );
    }
}

export default connect(state => state.slideShow)(SlideShow);
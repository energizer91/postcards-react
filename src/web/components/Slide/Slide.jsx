import React from 'react';

import './Slide.css';

const Slide = ({image, offsetX, offsetY}) => (
    <div className="slide">
        <div className="slide-image" style={{backgroundImage: 'url(' + image + ')', backgroundPositionX: (offsetX || 0) + '%', backgroundPositionY: (offsetY || 0) + '%'}} />
    </div>
);

export default Slide;
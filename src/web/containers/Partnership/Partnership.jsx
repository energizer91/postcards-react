/**
 * Created by Александр on 11.04.2017.
 */

import React from 'react';
import SlideShow from '../../components/SlideShow/SlideShow';
import slide1 from './images/01.jpg';
import slide2 from './images/02.jpg';

import './Partnership.css';
import './responsive/portrait_360.css';
import './responsive/landscape_360.css';
import './responsive/portrait_768.css';
import './responsive/landscape_768.css';
import './responsive/landscape_1366.css';
import './responsive/landscape_1600.css';
import './responsive/landscape_1920.css';

class Partnership extends React.Component {
    render() {
        const data = [
            {
                image: slide1,
                offsetY: 25
            },
            {
                image: slide2,
                offsetY: 55
            },
        ];
        return (
            <div className="wrapper partnership">
                <div className="slider">
                    <SlideShow data={data} />
                </div>
                <div className="text">
                    <div className="wrapper">
                        <div className="cell">
                            <h1>Приятное партнерство</h1>
                            <p>
                                Мы знаем, что многим людям, с поводом и без, наши открытки дарят незабываемые эмоции каждый день, поэтому нас любят и выбирают цветочные лавки, магазины сувениров и подарков, организаторы свадебных мероприятий и многие многие другие.
                            </p><p>
                            Хотите вместе с нами помогать передавать людям эмоции:<br />
                            <a href="mailto:mail@novieotritki.ru">mail@novieotritki.ru</a>
                        </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Partnership;
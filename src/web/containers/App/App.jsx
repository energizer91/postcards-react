import React from 'react';
import Header from '../../components/Header/Header';
import TransitionGroup from 'react-addons-transition-group';
import TransitionAnimation from '../../components/TransitionAnimation/TransitionAnimation';
import { connect } from 'react-redux';

import './App.css';

class Wrapper extends React.Component {
    render() {
        return (
            <div style={{minHeight: '100%'}}>
                {renderChildren(this.props)}
            </div>
        )
    }
}

function renderChildren(props) {
    return React.Children.map(props.children, child => {
        const newProps = Object.assign({}, props);
        delete newProps.children;
        return React.cloneElement(child || null, {...newProps});
    })
}

class App extends React.Component {
    render() {
        return (
            <div style={{minHeight: '100%'}}>
                <Header />
                <TransitionAnimation />
                <TransitionGroup component={connect(state => state)(Wrapper)}>
                    {this.props.children || null}
                </TransitionGroup>
            </div>
        )
    }
}

export default connect()(App);

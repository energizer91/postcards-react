import React from 'react';
import Contacts from '../../components/Contacts/Contacts';
import MainMenuHeading from '../../components/MainMenuHeading/MainMenuHeading';

import './Emotions.css';

class Emotions extends React.Component {
    render() {
        return (
            <div className="wrapper emotions">
                <MainMenuHeading title={["Как рождаются", "эмоции"]}/>
                <div className="container-element">
                    <Contacts leftText={'Left text'} rightText={'Right text'} leftStyle={{backgroundColor: 'white'}} rightStyle={{backgroundColor: 'yellow'}}/>
                </div>
            </div>
        )
    }
}

export default Emotions;
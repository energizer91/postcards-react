/**
 * Created by Александр on 19.04.2017.
 */

import { CHOOSE_CITY } from './constants';

export function chooseCity (city) {
    return {
        type: CHOOSE_CITY,
        currentCity: city
    };
}
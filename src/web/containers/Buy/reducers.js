/**
 * Created by Александр on 19.04.2017.
 */

import { CHOOSE_CITY } from './constants';

export default function buy (state = {currentCity: 0}, action) {
    switch (action.type) {
        case CHOOSE_CITY:
            return Object.assign({}, state, {
                currentCity: action.currentCity
            });
        default:
            return state;
    }
}
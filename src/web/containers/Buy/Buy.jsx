/**
 * Created by Александр on 11.04.2017.
 */

import React from 'react';
import Map from '../../components/Map/Map';
import { chooseCity } from './actions';
import { connect } from 'react-redux';

import './Buy.css';
import './responsive/portrait_360.css';
import './responsive/landscape_360.css';
import './responsive/portrait_768.css';
import './responsive/landscape_768.css';
import './responsive/landscape_1366.css';
import './responsive/landscape_1600.css';
import './responsive/landscape_1920.css';

const cities = [
    {
        name: 'Казань',
        lat: 55.7897386,
        lon: 49.1169024,
        zoom: 15,
        points: [
            {
                lat: 55.790104,
                lon: 49.109034,
                properties: {
                    hintContent: "Магазин 1",
                    balloonContentHeader: "Магазин 1",
                    balloonContentBody: "Описание 1"
                }
            },
            {
                lat: 55.792597,
                lon: 49.117126,
                properties: {
                    hintContent: "Магазин 2",
                    balloonContentHeader: "Магазин 2",
                    balloonContentBody: "Описание 2"
                }
            }
        ]
    },
    {
        name: 'Москва',
        lat: 55.7539256,
        lon: 37.6204687,
        zoom: 15,
        points: []
    },
    {
        name: 'Нижний Новгород',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    },
    {
        name: 'Ульяновск',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    },
    {
        name: 'Санкт-Петербург',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    },
    {
        name: 'Ижевск',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    },
    {
        name: 'Толятти',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    },
    {
        name: 'Самара',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    },
    {
        name: 'Нижнекамск',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    },
    {
        name: 'Калининград',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    },
    {
        name: 'Набережные Челны',
        lat: 0,
        lon: 0,
        zoom: 17,
        points: []
    }
];

class Partnership extends React.Component {
    constructor(props) {
        super(props);

        this.onCityClick = this.onCityClick.bind(this);
    }

    onCityClick(index) {
        this.props.dispatch(chooseCity(index));
    }

    render() {
        return (
            <div className="wrapper buy">
                <div className="map-wrapper">
                    <Map geoData={cities[this.props.currentCity]}/>
                </div>
                <div className="text">
                    <div className="wrapper">
                        <div className="cell">
                            <h1>Где купить открытку?</h1>
                            <ul className="cities">
                                {cities.map((city, index) => (
                                    <li key={index} className={index === this.props.currentCity ? 'selected' : ''} onClick={this.onCityClick.bind(this, index)}>{city.name}</li>
                                ))}
                            </ul>
                            <p>
                                Хотите вместе с нами помогать передавать людям эмоции:<br />
                                <a href="mailto:mail@novieotritki.ru">mail@novieotritki.ru</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => state.buy)(Partnership);
import React from 'react';
import { setActiveMenu } from '../../components/Header/actions';
import { TweenMax } from 'gsap';

import './NavigationTemplate.css';

class NavigationTemplate extends React.Component {
    componentDidMount() {
        if (this.props.headerMenu && !this.props.headerMenu.path) {
            this.props.dispatch(setActiveMenu(this.props.location.pathname));
        }
    }

    componentWillLeave(callback) {
        TweenMax.to(this.container, 0.2, {
            opacity: 0,
            clearProps: 'opacity',
            onComplete: callback
        })
    }

    render() {
        return (
            <div className="navigation-template" key="navigationTemplate" ref={c => this.container = c}>
                {this.props.children}
            </div>
        )
    }
}

export default NavigationTemplate;
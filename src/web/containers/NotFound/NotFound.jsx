import React from 'react';

class NotFound extends React.Component {
    render() {
        return (
            <div key="notFound" ref={c => this.container = c}>Not found</div>
        )
    }
}

export default NotFound;
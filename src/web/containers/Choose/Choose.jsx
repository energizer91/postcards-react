import React from 'react';

import './Choose.css';

class Emotions extends React.Component {
    render() {
        return (
            <div className="wrapper choose">
                <div className="main-bkg">
                    <h2 className="title">Выбери свою открытку</h2>
                </div>
            </div>
        )
    }
}

export default Emotions;
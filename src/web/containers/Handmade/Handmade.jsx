import React from 'react';
import Contacts from '../../components/Contacts/Contacts';
import MainMenuHeading from '../../components/MainMenuHeading/MainMenuHeading';

import './Handmade.css';

class Handmade extends React.Component {
    render() {
        return (
            <div className="wrapper handmade">
                <MainMenuHeading title={["Ручная работа", "художников"]}/>
                <div className="container-element">
                    <Contacts leftText={'Left text'} rightText={'Right text'} leftStyle={{backgroundColor: 'white'}} rightStyle={{backgroundColor: 'red'}}/>
                </div>
            </div>
        )
    }
}

export default Handmade;
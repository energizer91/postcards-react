import React from 'react';
import { setActiveMenu } from '../../components/Header/actions';
import { TimelineMax, TweenMax, Expo } from 'gsap';
import * as VirtualScroll from 'virtual-scroll';
import Arrow from '../../components/Arrow/Arrow';

import './MainTemplate.css';

class MainTemplate extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: 0,
            downAnchor: 0,
            windowHeight: 0,
            elements: [],
            keyStep: 75
        };

        this.scrollInstance = null;

        this.scrollHandler = this.scrollHandler.bind(this);
        this.resizeHandler = this.resizeHandler.bind(this);
        this.scrollDown = this.scrollDown.bind(this);
    }

    componentDidMount() {
        if (this.props.headerMenu && !this.props.headerMenu.path) {
            this.props.dispatch(setActiveMenu(this.props.location.pathname));
        }

        this.scrollInstance = new (VirtualScroll.default)({
            keyStep: this.state.keyStep
        });

        this.scrollInstance.on(this.scrollHandler);

        window.addEventListener('resize', this.resizeHandler);

        this.resizeHandler();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resizeHandler);
        this.scrollInstance.destroy();
    }

    resizeHandler() {
        if (this.container) {
            this.setState({
                downAnchor: this.downAnchor.getBoundingClientRect().bottom,
                windowHeight: window.innerHeight,
                elements: Array.prototype.map.call(this.container.getElementsByClassName('container-element'), (element) => {
                    const rect = element.getBoundingClientRect();

                    return {
                        element,
                        rect
                    };
                })
            });
        }
    }

    scrollHandler({deltaY}) {
        if (window.innerWidth < 1200) {
            return;
        }

        const scrollY = Math.min(Math.max(this.state.scrollY + deltaY, -this.state.downAnchor + window.innerHeight), 0);

        if (this.container) {
            const immutableElements = [].concat(this.state.elements);
            const nearElements = [];
            const affectedElements = [];
            const otherElements = [];
            const visibleBorders = {
                top: -scrollY,
                bottom: -scrollY + this.state.windowHeight
            };

            immutableElements.forEach((el) => {
                if (el.rect.top < visibleBorders.bottom + Math.abs(deltaY) && el.rect.bottom > visibleBorders.top + deltaY) {
                    affectedElements.push(el.element);
                } else if (Math.abs(visibleBorders.top - el.rect.bottom) <= this.state.keyStep) {
                    nearElements.push(el.element);
                }  else if (el.rect.bottom < visibleBorders.top) {
                    otherElements.push(el.element);
                } else if (el.rect.top - visibleBorders.bottom < this.state.keyStep) {
                    nearElements.push(el.element);
                } else {
                    otherElements.push(el.element);
                }
            });

            affectedElements.push(this.container.getElementsByClassName('title-container'));

            //parallaxing background
            TweenMax.to(this.container.getElementsByClassName('background-element'), 1, {
                y: scrollY * 0.8,
                ease: Expo.easeOut
            });
            //moving near objects to scroll position
            TweenMax.set(nearElements, {
                y: scrollY
            });
            //scrolling visible objects
            TweenMax.to(affectedElements, 1, {
                y: scrollY,
                ease: Expo.easeOut
            });
            //setting default to invisible objects
            TweenMax.set(otherElements, {
                y: 0
            });
        }

        this.setState({
            scrollY
        });
    }

    componentWillLeave(callback) {
        if (this.container.offsetWidth < 768) {
            const tl = new TimelineMax({onComplete: callback}),
                transitionWrapper = document.getElementById('transitionAnimation'),
                transitionInner = transitionWrapper.querySelector('.inner'),
                speed = 1;

            tl.set(transitionWrapper, {
                opacity: 1,
                display: 'block',
                yPercent: -100
            });
            tl.set(transitionInner.querySelector('.text-border'), {
                yPercent: 0,
                opacity: 1
            });
            tl.set(transitionInner, {yPercent: 0});

            tl.to(transitionWrapper, speed / 2, {yPercent: 0});
        } else {
            const tl = new TimelineMax({onComplete: callback}),
                speed = 1;
            const textBlock = document.querySelectorAll('.smooth-text-block .word');
            tl.to(this.container.getElementsByClassName('background-element'), speed, {yPercent: 0});
            tl.staggerTo(textBlock, speed, {
                yPercent: -100,
                ease: Expo.easeInOut
            }, 0.02, speed / 2);
        }
    }

    scrollDown() {
        this.scrollHandler({deltaY: -this.state.elements[0].rect.top});
    }

    render() {
        return (
            <div className="main-template" key="mainTemplate" ref={c => this.container = c}>
                <div className={this.state.scrollY === 0 ? 'scroll-arrow' : 'scroll-arrow hidden'}>
                    <Arrow onClick={this.scrollDown} imageStyle={{transform: 'rotate(-90deg)'}} rotation="90"/>
                </div>
                {this.props.children}
                <div className="down_anchor" ref={c => this.downAnchor = c} />
            </div>
        )
    }
}

export default MainTemplate;
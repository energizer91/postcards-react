import App from './App/App';
import HomePage from './HomePage/HomePage';
import NotFound from './NotFound/NotFound';
import Postcards from './Postcards/Postcards';
import Emotions from './Emotions/Emotions';
import Handmade from './Handmade/Handmade';
import Authors from './Authors/Authors';
import Partnership from './Partnership/Partnership';
import Buy from './Buy/Buy';
import MainTemplate from './MainTemplate/MainTemplate';
import NavigationTemplate from './NavigationTemplate/NavigationTemplate';

const routes = {
    component: App,
    childRoutes: [
        {
            path: '/',
            component: HomePage,
            key: 'mainMenu'
        },
        {
            component: MainTemplate,
            key: 'mainTemplate',
            childRoutes: [
                {
                    path: '/postcards',
                    component: Postcards
                },
                {
                    path: '/emotions',
                    component: Emotions
                },
                {
                    path: '/handmade',
                    component: Handmade
                }
            ]
        },
        {
            component: NavigationTemplate,
            key: 'navigationTemplate',
            childRoutes: [
                {
                    path: '/buy',
                    component: Buy
                },
                {
                    path: '/authors',
                    component: Authors
                },
                {
                    path: '/partnership',
                    component: Partnership
                }
            ]
        },
        {
            path: '*',
            key: 'notFound',
            component: NotFound
        }
    ]
};

export default routes;
/**
 * Created by Александр on 11.04.2017.
 */

import React from 'react';
import SlideShow from '../../components/SlideShow/SlideShow';
import slide1 from './images/01.jpg';
import slide2 from './images/02.jpg';

import './Authors.css';
import './responsive/portrait_360.css';
import './responsive/landscape_360.css';
import './responsive/portrait_768.css';
import './responsive/landscape_768.css';
import './responsive/landscape_1366.css';
import './responsive/landscape_1600.css';
import './responsive/landscape_1920.css';

class Authors extends React.Component {
    render() {
        const data = [
            {
                image: slide1,
                offsetX: 50,
                offsetY: 25
            },
            {
                image: slide2,
                offsetX: 100,
                offsetY: 55
            },
        ];
        return (
            <div className="wrapper authors">
                <div className="slider">
                    <SlideShow data={data} />
                </div>
                <div className="text">
                    <div className="wrapper">
                        <div className="cell">
                            <h1>Работа<br />с авторами</h1>
                            <p>
                                Мы вместе с нашими авторами вкладываем эмоции в создание открыток. Каждая иллюстрация, леттеринг или каллиграфия созданные с душой, помогают людям передать нужные эмоции.
                            </p>
                            <p>
                                Хотите вместе с нами создавать эмоции:<br />
                                <a href="mailto:mail@novieotritki.ru">mail@novieotritki.ru</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Authors;
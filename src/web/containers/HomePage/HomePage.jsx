import React from 'react';
import MainMenuListItem from '../../components/MainMenuListItem/MainMenuListItem';
import { TimelineMax, Expo, TweenMax } from 'gsap';
import 'gsap/ScrollToPlugin';
import { setActiveMenu } from '../../components/Header/actions';

import './HomePage.css';

const items = [
    {
        className: 'postcards',
        path: '/postcards',
        title: 'Выбери свою открытку',
        centerOffset: 42,
        subTitle: 'Дарите уникальные открытки, сделанные с душой!'
    },
    {
        className: 'emotions',
        path: '/emotions',
        title: 'Как рождаются эмоции',
        centerOffset: 54,
        subTitle: 'Печать по технологии Letterpress на уникальном оборудовании XIX и XX века.'
    },
    {
        className: 'handmade',
        path: '/handmade',
        title: 'Ручная работа художника',
        centerOffset: 47,
        subTitle: 'Мастера каллиграфии и леттеринга помогают передать ваши чувства и внимание.'
    }
];
const scaleFactor = .667;

function findMenuIndex (path) {
    for (let i = 0; i < items.length; i++) {
        if (items[i].path === path) {
            return i;
        }
    }
    return -1;
}

function divideMenu(menus, path) {
    const selectedIndex = findMenuIndex(path);
    return {
        leftMenus: Array.prototype.slice.call(menus, 0, selectedIndex).reverse(),
        selected: menus[selectedIndex],
        rightMenus: Array.prototype.slice.call(menus, selectedIndex + 1, menus.length)
    };
}

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.onResize = this.onResize.bind(this);
    }

    handleClick(path) {
        this.props.dispatch(setActiveMenu(path));
    }

    componentWillEnter (callback) {
        const menus = this.container.childNodes;
        const {leftMenus, selected, rightMenus} = divideMenu(menus, this.props.headerMenu.path);

        if (!selected) {
            return callback();
        }

        const tl = new TimelineMax({onComplete: callback}),
            transitionWrapper = document.getElementById('transitionAnimation');

        if (this.container.offsetWidth >= 767) {
            const blockWidth = selected.offsetWidth,
                transitionInner = transitionWrapper.querySelector('.inner'),
                transitionImage = transitionWrapper.querySelector('.image'),
                speed = 1;

            //tl.set(document.body, {overflow: 'hidden'});
            tl.set(transitionInner.querySelector('.title'), {y: 50});
            tl.set(transitionInner.querySelector('.sub-title'), {yPercent: -100, opacity: 0});
            tl.set(selected, {opacity: 0});

            transitionInner.classList = selected.classList;

            tl.add('fadeOut', '+=' + (speed + speed / 2));

            tl.set(transitionWrapper, {opacity: 1, display: 'block'}, 'fadeOut');
            tl.set(menus, {opacity: 1}, 'fadeOut');

            transitionInner.querySelector('.title').innerText = selected.querySelector('.title').innerText;
            transitionInner.querySelector('.sub-title-inner p').innerText = selected.querySelector('.sub-title-inner p').innerText;

            transitionImage.style.backgroundPositionX = selected.dataset.centerset + '%';

            tl.set(transitionInner.querySelector('.text-border'), {
                xPercent: -100,
                yPercent: 50,
                opacity: 0
            });

            tl.staggerFromTo(leftMenus, speed, {
                cycle: {
                    x: index => {
                        if (index <= 3) {
                            const offset = leftMenus[index].getBoundingClientRect().left;
                            return -blockWidth * (index + 1)  - offset;
                        }
                    }
                }
            }, {
                x: 0,
                ease: Expo.easeInOut
            }, 0, 'fadeOut');
            tl.staggerFromTo(rightMenus, speed, {
                cycle: {
                    x: index => {
                        if (index <= 3) {
                            const offset = document.body.offsetWidth - rightMenus[index].getBoundingClientRect().left;
                            return blockWidth * index + offset;
                        }
                    }
                }
            }, {
                x: 0,
                ease: Expo.easeInOut
            }, 0, 'fadeOut');

            tl.to(transitionInner.querySelector('.text-border'), speed, {xPercent: 0, yPercent: 0, ease: Expo.easeInOut}, 'fadeOut');
            tl.to(transitionInner.querySelector('.text-border'), speed / 2, {opacity: 1, ease: Expo.easeInOut}, 'fadeOut');
            tl.fromTo(transitionImage, speed, {
                xPercent: 0,
                scale: 1
            }, {
                xPercent:  scaleFactor * (selected.dataset.centerset * -1 + 100),
                scale: scaleFactor,
                ease: Expo.easeInOut
            }, 'fadeOut');
            tl.to(transitionWrapper, speed, {x: selected.getBoundingClientRect().left, ease: Expo.easeInOut}, 'fadeOut');
            tl.to(transitionInner, speed, {xPercent: -66.666667, ease: Expo.easeInOut}, 'fadeOut');
            tl.to(selected, speed / 2, {opacity: 1}, 'fadeOut');
            tl.to(transitionWrapper, speed / 2, {opacity: 'fadeOut'});
            tl.set(transitionWrapper, {display: 'none', opacity: 1, clearProps: 'all'});
            tl.set(transitionInner.querySelector('.title'), {clearProps: 'y'});
            tl.set(transitionInner.querySelector('.sub-title'), {clearProps: 'all'});
            //tl.set(document.body, {clearProps: 'overflow'});
        } else {
            const blockHeight = selected.clientHeight,
                transitionInner = transitionWrapper.querySelector('.inner'),
                speed = 1;

            transitionInner.classList = selected.classList;

            transitionInner.querySelector('.title').innerText = selected.querySelector('.title').innerText;
            transitionInner.querySelector('.sub-title-inner p').innerText = selected.querySelector('.sub-title-inner p').innerText;

            tl.set(document.body, {overflow: 'hidden'});
            tl.set(this.container, {opacity: 0});

            tl.add('fadeOut', '+=' + speed / 2);

            tl.set(window, {scrollTo: selected.offsetTop, onComplete: () => {
                tl.to(transitionWrapper, speed, {y: selected.offsetTop - document.body.scrollTop, ease: Expo.easeInOut}, 'fadeOut');
                tl.to(transitionInner, speed, {yPercent: -11, ease: Expo.easeInOut}, 'fadeOut');

                tl.set(this.container, {opacity: 1}, 'fadeOut');

                tl.staggerFromTo(leftMenus, speed, {
                    cycle: {
                        y: index => {
                            if (index <= 3) {
                                const offset = leftMenus[index].getBoundingClientRect().top;
                                return -blockHeight * (index + 1)  - offset;
                            }
                        }
                    },
                    ease: Expo.easeInOut
                }, {
                    y: 0,
                    ease: Expo.easeInOut
                } , 0, 'fadeOut');
                tl.staggerFromTo(rightMenus, speed, {
                    cycle: {
                        y: index => {
                            if (index <= 3) {
                                const offset = document.body.clientHeight - rightMenus[index].getBoundingClientRect().top;
                                return blockHeight * index + offset;
                            }
                        }
                    },
                    ease: Expo.easeInOut
                }, {
                    y: 0,
                    ease: Expo.easeInOut
                } , 0, 'fadeOut');

                tl.to(transitionWrapper, speed / 2, {opacity: 0});
                tl.set(transitionWrapper, {display: 'none', opacity: 1, clearProps: 'all'});
                tl.set(document.body, {clearProps: 'overflow'});
            }}, 'fadeOut');

        }
    }

    componentWillLeave (callback) {
        const el = this.container;
        console.log('WillLeave', el);

        const menus = this.container.childNodes;
        const {leftMenus, selected, rightMenus} = divideMenu(menus, this.props.headerMenu.path);
        const textBlock = document.querySelectorAll('.smooth-text-block .word');
        const scrollArrow = document.querySelector('.scroll-arrow');

        if (!selected) {
            return callback();
        }

        const tl = new TimelineMax({onComplete: callback}),
            transitionWrapper = document.getElementById('transitionAnimation');

        if (this.container.offsetWidth >= 767) {
            const blockWidth = selected.offsetWidth,
                transitionInner = transitionWrapper.querySelector('.inner'),
                transitionImage = transitionWrapper.querySelector('.image'),
                speed = 1;

            tl.set(document.body, {overflow: 'hidden'});
            tl.set(transitionWrapper, {
                x: selected.getBoundingClientRect().left
            });

            transitionInner.classList = selected.classList;

            tl.set(transitionInner, {
                xPercent: -66.6666666
            });

            transitionInner.querySelector('.title').innerText = selected.querySelector('.title').innerText;
            transitionInner.querySelector('.sub-title-inner p').innerText = selected.querySelector('.sub-title-inner p').innerText;

            transitionImage.style.backgroundPositionX = selected.dataset.centerset + '%';

            tl.set(transitionWrapper, {display: 'block'}, 0);
            tl.set(selected, {opacity: 0});
            tl.set(textBlock, {yPercent: 100});

            tl.staggerTo(leftMenus, speed, {
                cycle: {
                    x: index => {
                        if (index <= 3) {
                            const offset = leftMenus[index].getBoundingClientRect().left;
                            return -blockWidth * (index + 1)  - offset;
                        }
                    }
                },
                ease: Expo.easeInOut
            }, 0, 0);
            tl.staggerTo(rightMenus, speed, {
                cycle: {
                    x: index => {
                        if (index <= 3) {
                            const offset = document.body.offsetWidth - rightMenus[index].getBoundingClientRect().left;
                            return blockWidth * index + offset;
                        }
                    }
                },
                ease: Expo.easeInOut
            }, 0, 0);
            tl.to(transitionInner.querySelector('.text-border'), speed, {
                xPercent: -100,
                yPercent: 50,
                ease: Expo.easeInOut
            }, 0);
            tl.to(transitionInner.querySelector('.text-border'), speed / 2, {
                opacity: 0,
                ease: Expo.easeInOut
            }, 0);
            tl.fromTo(transitionImage, speed, {
                xPercent:  scaleFactor * (selected.dataset.centerset * -1 + 100),
                scale: scaleFactor
            }, {
                xPercent: 0,
                scale: 1,
                ease: Expo.easeInOut
            }, 0);
            tl.to(transitionWrapper, speed, {
                x: 0,
                ease: Expo.easeInOut
            }, 0);
            tl.fromTo(transitionInner, speed, {
                backgroundPositionX: selected.getBoundingClientRect().left + 'px'
            }, {
                backgroundPositionX: 0,
                ease: Expo.easeInOut
            }, 0);
            tl.to(transitionInner, speed, {
                xPercent: 0,
                ease: Expo.easeInOut
            }, 0);
            tl.set(menus, {opacity: 0});
            tl.to(transitionWrapper, speed / 2, {opacity: 0});
            tl.staggerTo(textBlock, speed, {
                yPercent: 0,
                ease: Expo.easeInOut
            }, 0.02);
            tl.to(scrollArrow, speed / 4, {opacity: 1});
            tl.set(transitionWrapper, {display: 'none', clearProps: 'opacity'});
            tl.set(document.body, {clearProps: 'overflow'});
        } else {
            const blockHeight = selected.clientHeight,
                transitionInner = transitionWrapper.querySelector('.inner'),
                transitionImage = transitionWrapper.querySelector('.image'),
                speed = 1;

            tl.set(document.body, {overflow: 'hidden'});

            transitionInner.classList = selected.classList;

            tl.set(transitionInner, {yPercent: -11}, 0);
            tl.set(transitionWrapper, {
                display: 'block',
                y: selected.offsetTop - document.body.scrollTop
            }, 0);

            transitionInner.querySelector('.title').innerText = selected.querySelector('.title').innerText;
            transitionInner.querySelector('.sub-title-inner p').innerText = selected.querySelector('.sub-title-inner p').innerText;

            transitionImage.style.backgroundPositionX = selected.dataset.centerset + '%';

            tl.staggerTo(leftMenus, speed, {
                cycle: {
                    y: index => {
                        if (index <= 3) {
                            const offset = leftMenus[index].getBoundingClientRect().top;
                            return -blockHeight * (index + 1)  - offset;
                        }
                    }
                },
                ease: Expo.easeInOut
            }, 0, 0);
            tl.staggerTo(rightMenus, speed, {
                cycle: {
                    y: index => {
                        if (index <= 3) {
                            const offset = document.body.clientHeight - rightMenus[index].getBoundingClientRect().top;
                            return blockHeight * index + offset;
                        }
                    }
                },
                ease: Expo.easeInOut
            }, 0, 0);
            tl.to(transitionWrapper, speed, {
                y: 0,
                ease: Expo.easeInOut
            }, 0);
            tl.to(transitionInner, speed, {
                yPercent: 0,
                ease: Expo.easeInOut
            }, 0);
            tl.to(transitionInner.querySelector('.text-border'), speed, {
                yPercent: 50,
                ease: Expo.easeInOut
            }, 0);
            tl.to(transitionInner.querySelector('.text-border'), speed / 2, {
                opacity: 0,
                ease: Expo.easeInOut
            }, 0);
            tl.fromTo(transitionImage, speed, {
                xPercent:  0.89 * (selected.dataset.centerset * -1 + 100),
                scale: 0.89
            }, {
                xPercent: 0,
                scale: 1,
                ease: Expo.easeInOut
            }, 0);
            tl.set(this.container, {opacity: 0});
            tl.set(window, {scrollTo: 0});
            tl.to(transitionWrapper, speed / 2, {opacity: 0});
            tl.set(transitionWrapper, {display: 'none', clearProps: 'opacity'});
            tl.set(document.body, {clearProps: 'overflow'});
        }
    }

    onResize() {
        const menus = this.container.childNodes,
            windowWidth = window.innerWidth;

        TweenMax.staggerTo(this.container.querySelectorAll('.image'), 0, {
            cycle: {
                x: function (index) {
                    return windowWidth * scaleFactor / 100 * menus[index].dataset.centerset * -1;
                }
            }
        });
    }

    componentDidMount() {
        window.addEventListener('resize', this.onResize);
        this.onResize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
    }

    getMainMenu() {
        return items.map((item, i) => {
            item.scaleFactor = scaleFactor;
            return <MainMenuListItem {...item} key={i} handleClick={this.handleClick}/>
        })
    }

    render() {
        return (
            <ul className="main-menu" style={{height: '100%', fontSize: 0}} key="main-menu" ref={c => this.container = c}>
                {this.getMainMenu()}
            </ul>
        )
    }
}

export default HomePage;
import React from 'react';
import Contacts from '../../components/Contacts/Contacts';
import MainMenuHeading from '../../components/MainMenuHeading/MainMenuHeading';

import './Postcards.css';

class Postcards extends React.Component {
    render() {
        return (
            <div className="wrapper postcards">
                <MainMenuHeading title={["Выбери свою", "открытку"]}/>
                <div className="container-element">
                    <Contacts leftText={'Left text'} rightText={'Right text'} leftStyle={{backgroundColor: 'white'}} rightStyle={{backgroundColor: 'blue', color: 'white'}}/>
                </div>
            </div>
        )
    }
}

export default Postcards;
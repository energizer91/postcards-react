/**
 * Created by energizer on 25.04.17.
 */

import '../index.css';
import './Preloader.css';

import React from 'react';
import ReactDOM from 'react-dom';

const loadPromise = new Promise(function (resolve) {
    window.onload = function () {
        resolve(true);
    };
});

const SmileVector = ({extraClass, dashOffset, smilesText}) => (
    <svg className={['smile_logo'].concat(extraClass).join(' ')} viewBox="0 0 490 144">
        <g className="smile">
            <path d="M0,0c42.2,85.1,135.9,144.2,244.8,144.2" style={{strokeDashoffset: dashOffset + 'px'}}/>
            <path d="M489.4,0c-42.2,85.1,-135.9,144.2,-244.8,144.2" style={{strokeDashoffset: dashOffset + 'px'}}/>
            <text x="50%" y="250" className="percentText" >{smilesText}</text>
        </g>
    </svg>
);

const LoaderList = ({items, smilesOffset, smilesText}) => (
    <ul className="loader-parts">
        {items.map((item, index) => (
            <li key={index}>
                <div className="wrapper">
                    <SmileVector
                        extraClass={item}
                        dashOffset={smilesOffset}
                        smilesText={smilesText}
                    />
                </div>
            </li>
        ))}
    </ul>
);

const loaderElement = document.getElementById('preloader');

class Preloader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            counter: 0,
            step: 5,
            percentInterval: 0,
            smilesOffset: 300,
            smilesText: '',
            loaded: false
        };

        this.intervalFn = this.intervalFn.bind(this);
    }

    animateLoadingProcess(current, total, dataLoaded) {
        let progress = 100 * current / total;
        const newState = {
            counter: current,
            smilesOffset: 300 + 3 * progress,
            loaded: dataLoaded
        };

        if (!dataLoaded) {
            newState.smilesText = progress + '%';
        }

        this.setState(newState);
    }

    intervalFn() {
        if (this.state.loaded) {
            loadPromise.then(function () {
                document.body.classList.remove('loading');
                document.body.classList.add('loaded');
                setTimeout(function () {
                    ReactDOM.unmountComponentAtNode(loaderElement);
                    document.body.removeChild(loaderElement);
                }, 4600);
            });

            return clearInterval(this.state.percentInterval);
        } else if (this.state.counter === 100) {
            return this.animateLoadingProcess(this.state.counter - this.state.step, 100, true);
        }

        this.animateLoadingProcess(this.state.counter + this.state.step, 100);
    }

    componentDidMount() {
        document.body.classList.add('loading');
        if (!this.state.percentInterval) {
            this.setState({
                percentInterval: setInterval(this.intervalFn, 3000 / (100 / this.state.step))
            });
        }
    }

    render() {
        return <LoaderList
            items={['first', 'second', 'third']}
            smilesOffset={this.state.smilesOffset}
            smilesText={this.state.smilesText}
        />
    }
}

ReactDOM.render(
    <Preloader/>,
    loaderElement
);